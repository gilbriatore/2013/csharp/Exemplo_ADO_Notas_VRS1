﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Alunos
{
    public partial class frmCadAlunos : Form
    {
        SqlConnection cnn;
        public frmCadAlunos()
        {
            InitializeComponent();
        }

        private void frmCadAlunos_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bancoAlunosDataSet1.Alunos' table. You can move, or remove it, as needed.
            this.alunosTableAdapter.Fill(this.bancoAlunosDataSet1.Alunos);
            string conexao = "Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\UP\\C#\\Exemplo_ADO_Notas_VRS1\\BancoAlunos.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True";
            cnn = new SqlConnection(conexao);
            cnn.Open();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandText = "SELECT al_Id FROM Alunos WHERE al_Nome = '" + txtNome.Text + "'";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                dr.Close();
                MessageBox.Show("Já cadastrado.");
            }
            else
            {
                dr.Close();
                cmd.CommandText = "INSERT INTO Alunos (al_Nome, al_B1, al_B2, al_B3, al_B4) " +
                              "VALUES (@al_Nome, @al_B1, @al_B2, @al_B3, @al_B4)";
                cmd.Parameters.AddWithValue("@al_Nome", txtNome.Text);
                cmd.Parameters.AddWithValue("@al_B1", float.Parse(txtB1.Text));
                cmd.Parameters.AddWithValue("@al_B2", float.Parse(txtB2.Text));
                cmd.Parameters.AddWithValue("@al_B3", float.Parse(txtB3.Text));
                cmd.Parameters.AddWithValue("@al_B4", float.Parse(txtB4.Text));
                cmd.ExecuteNonQuery();
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = alunosTableAdapter.GetData();
            }
        }

        private void btnRegistros_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandText = "SELECT * FROM Alunos WHERE al_Id = 14 AND al_Nome = 'Karina' ORDER BY al_Nome";
            SqlDataReader dr = cmd.ExecuteReader();
            while(dr.Read())
            {
                MessageBox.Show("Aluno: " + dr["al_Id"] + " - " + dr["al_Nome"]);
            }
            dr.Close();
        }

        private void btnFiltro_Click(object sender, EventArgs e)
        {
            string cmdSQL = "SELECT * FROM Alunos WHERE al_Nome LIKE '%" + txtNome.Text + "%' ORDER BY al_Nome";
            SqlDataAdapter da = new SqlDataAdapter(cmdSQL, cnn);
            DataSet ds = new DataSet();
            da.Fill(ds, "Alunos");
            dataGridView1.DataSource = ds.Tables["Alunos"];
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandText = "SELECT al_Id FROM Alunos WHERE al_Nome = '" + txtNome.Text + "'";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                dr.Close();
                cmd.CommandText = "UPDATE Alunos SET al_B1 = @al_B1, al_B2 = @al_B2, al_B3 = @al_B3, al_B4 = @al_B4 " +
                                  "WHERE al_Nome = '" + txtNome.Text + "'";
                cmd.Parameters.AddWithValue("@al_B1", float.Parse(txtB1.Text));
                cmd.Parameters.AddWithValue("@al_B2", float.Parse(txtB2.Text));
                cmd.Parameters.AddWithValue("@al_B3", float.Parse(txtB3.Text));
                cmd.Parameters.AddWithValue("@al_B4", float.Parse(txtB4.Text));
                cmd.ExecuteNonQuery();
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = alunosTableAdapter.GetData();
            }
            else
            {
                dr.Close();
                MessageBox.Show("Não cadastrado.");
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandText = "SELECT al_Id FROM Alunos WHERE al_Nome = '" + txtNome.Text + "'";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                dr.Close();
                cmd.CommandText = "DELETE FROM Alunos WHERE al_Nome = '" + txtNome.Text + "'";
                cmd.ExecuteNonQuery();
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = alunosTableAdapter.GetData();
            }
            else
            {
                dr.Close();
                MessageBox.Show("Não cadastrado.");
            }
        }
        
    }
}
