﻿namespace Alunos
{
    partial class frmCadAlunos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpDados = new System.Windows.Forms.GroupBox();
            this.txtB4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtB3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtB2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtB1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.btnRegistros = new System.Windows.Forms.Button();
            this.btnFiltro = new System.Windows.Forms.Button();
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bancoAlunosDataSet1 = new Alunos.BancoAlunosDataSet1();
            this.alunosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.alunosTableAdapter = new Alunos.BancoAlunosDataSet1TableAdapters.AlunosTableAdapter();
            this.alIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alNomeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alB1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alB2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alB3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alB4DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpDados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bancoAlunosDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alunosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // grpDados
            // 
            this.grpDados.Controls.Add(this.txtB4);
            this.grpDados.Controls.Add(this.label4);
            this.grpDados.Controls.Add(this.txtB3);
            this.grpDados.Controls.Add(this.label3);
            this.grpDados.Controls.Add(this.txtB2);
            this.grpDados.Controls.Add(this.label2);
            this.grpDados.Controls.Add(this.txtB1);
            this.grpDados.Controls.Add(this.label1);
            this.grpDados.Controls.Add(this.txtNome);
            this.grpDados.Controls.Add(this.lblNome);
            this.grpDados.Location = new System.Drawing.Point(12, 12);
            this.grpDados.Name = "grpDados";
            this.grpDados.Size = new System.Drawing.Size(416, 168);
            this.grpDados.TabIndex = 0;
            this.grpDados.TabStop = false;
            this.grpDados.Text = "Dados do aluno";
            // 
            // txtB4
            // 
            this.txtB4.Location = new System.Drawing.Point(96, 132);
            this.txtB4.MaxLength = 100;
            this.txtB4.Name = "txtB4";
            this.txtB4.Size = new System.Drawing.Size(56, 20);
            this.txtB4.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 8;
            this.label4.Text = "4. Bimestre";
            // 
            // txtB3
            // 
            this.txtB3.Location = new System.Drawing.Point(96, 104);
            this.txtB3.MaxLength = 100;
            this.txtB3.Name = "txtB3";
            this.txtB3.Size = new System.Drawing.Size(56, 20);
            this.txtB3.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 6;
            this.label3.Text = "3. Bimestre";
            // 
            // txtB2
            // 
            this.txtB2.Location = new System.Drawing.Point(96, 76);
            this.txtB2.MaxLength = 100;
            this.txtB2.Name = "txtB2";
            this.txtB2.Size = new System.Drawing.Size(56, 20);
            this.txtB2.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "2. Bimestre";
            // 
            // txtB1
            // 
            this.txtB1.Location = new System.Drawing.Point(96, 48);
            this.txtB1.MaxLength = 100;
            this.txtB1.Name = "txtB1";
            this.txtB1.Size = new System.Drawing.Size(56, 20);
            this.txtB1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "1. Bimestre";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(96, 20);
            this.txtNome.MaxLength = 100;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(300, 20);
            this.txtNome.TabIndex = 1;
            // 
            // lblNome
            // 
            this.lblNome.Location = new System.Drawing.Point(12, 24);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(100, 23);
            this.lblNome.TabIndex = 0;
            this.lblNome.Text = "Nome do aluno";
            // 
            // btnIncluir
            // 
            this.btnIncluir.Location = new System.Drawing.Point(444, 20);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(75, 28);
            this.btnIncluir.TabIndex = 1;
            this.btnIncluir.Text = "&Incluir";
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnRegistros
            // 
            this.btnRegistros.Location = new System.Drawing.Point(444, 56);
            this.btnRegistros.Name = "btnRegistros";
            this.btnRegistros.Size = new System.Drawing.Size(75, 28);
            this.btnRegistros.TabIndex = 3;
            this.btnRegistros.Text = "&Resgistros";
            this.btnRegistros.UseVisualStyleBackColor = true;
            this.btnRegistros.Click += new System.EventHandler(this.btnRegistros_Click);
            // 
            // btnFiltro
            // 
            this.btnFiltro.Location = new System.Drawing.Point(444, 92);
            this.btnFiltro.Name = "btnFiltro";
            this.btnFiltro.Size = new System.Drawing.Size(75, 28);
            this.btnFiltro.TabIndex = 4;
            this.btnFiltro.Text = "&Filtro";
            this.btnFiltro.UseVisualStyleBackColor = true;
            this.btnFiltro.Click += new System.EventHandler(this.btnFiltro_Click);
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.Location = new System.Drawing.Point(444, 128);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(75, 28);
            this.btnAtualizar.TabIndex = 5;
            this.btnAtualizar.Text = "&Atualizar";
            this.btnAtualizar.UseVisualStyleBackColor = true;
            this.btnAtualizar.Click += new System.EventHandler(this.btnAtualizar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(444, 164);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 28);
            this.btnExcluir.TabIndex = 6;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.alIDDataGridViewTextBoxColumn,
            this.alNomeDataGridViewTextBoxColumn,
            this.alB1DataGridViewTextBoxColumn,
            this.alB2DataGridViewTextBoxColumn,
            this.alB3DataGridViewTextBoxColumn,
            this.alB4DataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.alunosBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 204);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(508, 150);
            this.dataGridView1.TabIndex = 7;
            // 
            // bancoAlunosDataSet1
            // 
            this.bancoAlunosDataSet1.DataSetName = "BancoAlunosDataSet1";
            this.bancoAlunosDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // alunosBindingSource
            // 
            this.alunosBindingSource.DataMember = "Alunos";
            this.alunosBindingSource.DataSource = this.bancoAlunosDataSet1;
            // 
            // alunosTableAdapter
            // 
            this.alunosTableAdapter.ClearBeforeFill = true;
            // 
            // alIDDataGridViewTextBoxColumn
            // 
            this.alIDDataGridViewTextBoxColumn.DataPropertyName = "al_ID";
            this.alIDDataGridViewTextBoxColumn.HeaderText = "al_ID";
            this.alIDDataGridViewTextBoxColumn.Name = "alIDDataGridViewTextBoxColumn";
            this.alIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // alNomeDataGridViewTextBoxColumn
            // 
            this.alNomeDataGridViewTextBoxColumn.DataPropertyName = "al_Nome";
            this.alNomeDataGridViewTextBoxColumn.HeaderText = "al_Nome";
            this.alNomeDataGridViewTextBoxColumn.Name = "alNomeDataGridViewTextBoxColumn";
            // 
            // alB1DataGridViewTextBoxColumn
            // 
            this.alB1DataGridViewTextBoxColumn.DataPropertyName = "al_B1";
            this.alB1DataGridViewTextBoxColumn.HeaderText = "al_B1";
            this.alB1DataGridViewTextBoxColumn.Name = "alB1DataGridViewTextBoxColumn";
            // 
            // alB2DataGridViewTextBoxColumn
            // 
            this.alB2DataGridViewTextBoxColumn.DataPropertyName = "al_B2";
            this.alB2DataGridViewTextBoxColumn.HeaderText = "al_B2";
            this.alB2DataGridViewTextBoxColumn.Name = "alB2DataGridViewTextBoxColumn";
            // 
            // alB3DataGridViewTextBoxColumn
            // 
            this.alB3DataGridViewTextBoxColumn.DataPropertyName = "al_B3";
            this.alB3DataGridViewTextBoxColumn.HeaderText = "al_B3";
            this.alB3DataGridViewTextBoxColumn.Name = "alB3DataGridViewTextBoxColumn";
            // 
            // alB4DataGridViewTextBoxColumn
            // 
            this.alB4DataGridViewTextBoxColumn.DataPropertyName = "al_B4";
            this.alB4DataGridViewTextBoxColumn.HeaderText = "al_B4";
            this.alB4DataGridViewTextBoxColumn.Name = "alB4DataGridViewTextBoxColumn";
            // 
            // frmCadAlunos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 372);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnAtualizar);
            this.Controls.Add(this.btnFiltro);
            this.Controls.Add(this.btnRegistros);
            this.Controls.Add(this.btnIncluir);
            this.Controls.Add(this.grpDados);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadAlunos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Notas";
            this.Load += new System.EventHandler(this.frmCadAlunos_Load);
            this.grpDados.ResumeLayout(false);
            this.grpDados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bancoAlunosDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alunosBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDados;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.TextBox txtB2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtB1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtB4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtB3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.Button btnRegistros;
        private System.Windows.Forms.Button btnFiltro;
        private System.Windows.Forms.Button btnAtualizar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.DataGridView dataGridView1;
        private BancoAlunosDataSet1 bancoAlunosDataSet1;
        private System.Windows.Forms.BindingSource alunosBindingSource;
        private Alunos.BancoAlunosDataSet1TableAdapters.AlunosTableAdapter alunosTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn alIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn alNomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn alB1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn alB2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn alB3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn alB4DataGridViewTextBoxColumn;

    }
}

